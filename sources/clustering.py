import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import kneighbors_graph
import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist
from scipy.cluster.hierarchy import linkage, dendrogram
from scipy.cluster.hierarchy import fcluster
import math
from pandas.tools.plotting import scatter_matrix
from sklearn.metrics import silhouette_score
from sklearn.neighbors import kneighbors_graph
import matplotlib.patches as mpatches
import matplotlib
from random import randint
from collections import Counter
#import seaborn as sns # Decommentare per correlazione attributi

def main():
	input_filename = '../dataset/train_fixed.csv'
	df = pd.read_csv(input_filename, skipinitialspace=True)
	# Dataset in cui verranno aggiunti i cluster
	df_origin = pd.read_csv(input_filename,skipinitialspace=True)
	# Normalizzazione di tutti gli attributi del dataset
	df = ((df - df.mean()) / (df.max() - df.min()))

	# Correlazione tra gli attributi (Decommentare import seaborn)
	#correlation(df)
	
	# Calcolo dei cluster e aggiunta al dataset non normalizzato
	
	# ---- Hierarchical Single---- #
	clusters = hierarchical(df,"single",0.6)
	df_origin["Single"] = clusters
	percent_clusters(clusters)
	
	# ---- Hierarchical Complete ---- #
	clusters = hierarchical(df,"complete",1.65)
	df_origin["Complete"] = clusters
	percent_clusters(clusters)
	
	# ---- Hierarchical Average ---- #
	clusters = hierarchical(df,"average",0.93)
	df_origin["Average"] = clusters
	percent_clusters(clusters)
	
	# ---- K-Means ---- #
	clusters = kmeans(df)
	df_origin["Kmeans"] = clusters
	percent_clusters(clusters)
	
	# ---- DBSCAN ---- #
	clusters = dbscan(df)
	df_origin["Dbscan"] = clusters
	percent_clusters(clusters)

	#df_origin.to_csv('../dataset/train_clustered.csv',index=False)

	#scatterplot_data(df,clusters)
	
# CLUSTERING GERARCHICO
# df - DataFrame contenente il dataset
# strategy - Tipo di collegamento {complete,single,average}
# treshold - Distanza per il taglio del dendrogramma
def hierarchical(df,strategy,treshold):
	# Matrice delle distanze
	data_dist = pdist(df.values, metric='euclidean')

	# Creo dendrogramma con strategia "strategy"
	data_link = linkage(y=data_dist,method=strategy, metric="euclidean")
	d = dendrogram(data_link,no_labels=True)
	plt.title(strategy.title()+" Linkage")
	print(len(d["ivl"]))
	plt.plot([0,len(d["ivl"])*len(d["ivl"])],[treshold,treshold],"k-",lw=2,color="Red",linestyle="--",alpha=0.7,label="Threshold")
	plt.legend(fancybox=True, shadow=True,numpoints=1,bbox_to_anchor=(1.1, 1.05))
	plt.show()
	
	cutted_cluster = fcluster(data_link,treshold,"distance")

	return cutted_cluster
	
# CLUSTERING DBSCAN
# df - DataFrame contenente il dataset
def dbscan(df):
	# Matrice delle distanze per il calcolo di eps per 9 punti
	graph = kneighbors_graph(df,9,metric="euclidean",mode="distance")

	# Plot delle distanze in ordine crescente
	arr = []
	for m in np.asarray(graph.todense()):
		for dist in m:
			if dist > 0:
				arr.append(float(dist))
	arr.sort()
	plt.xlabel("Coppia di punti")
	plt.ylabel("Distanza")
	plt.title("K-Distance Graph")
	plt.plot(7500,0.35,marker="o",color="Red",label="eps")
	plt.plot(arr)
	plt.legend(fancybox=True, shadow=True,numpoints=1,handlelength=0)
	#plt.show()
	
	dbscan = DBSCAN(eps=0.35, min_samples=9, metric='euclidean').fit(df.values)
	print("DBSCAN Silihouette: ",silhouette_score(df.values,dbscan.labels_))

	return dbscan.labels_
	
# CLUSTERING KMEANS
# df - DataFrame contenente il dataset
# init_centroids - Centroidi iniziali
def kmeans(df,init_centroids = 0):
	if init_centroids != 0:
		# Init con centroidi dati
		n_cluster = len(init_centroids)
		c = np.array(init_centroids,np.int)
		kmeans = KMeans(n_clusters=n_cluster, n_init=1, init=c, max_iter=100).fit(df.values)
	else:
		# Init con k-means
		maxS = 0
		clusters_final = 17
		# Calcolo del k con silhouette più basso
		#for clusters in range(2,100):
			#kmeans = KMeans(init='k-means++', n_clusters=clusters, n_init=10, max_iter=100).fit(df.values)
			#silhouette = silhouette_score(df.values,kmeans.labels_)
			#clusters_final = clusters
			#if silhouette > maxS:
				#maxS = silhouette
				#clusters_final = clusters
			#print(maxS, clusters_final)
		#print("Risultato: ",maxS, clusters_final)
		kmeans = KMeans(init='k-means++', n_clusters=clusters_final, n_init=10, max_iter=100).fit(df.values)
		print("K-means Silihouette: ",silhouette_score(df.values,kmeans.labels_))

		return kmeans.labels_
		
# SCATTERPLOT MATRIX
# df - DataFrame contenente il dataset
# labels - Punti di df raggruppati in cluster
# area - Area dei punti da visualizzare nel plot
def scatterplot_matrix(df,labels,area):
	scatter_position=1
	scatter_matrix(df,c=labels, figsize=(6, 6), diagonal="kde",s=area)
	plt.show()

# SCATTERPLOT every attribute pairwise
# df - DataFrame contenente il dataset
# labels - Punti di df raggruppatti in cluster
def scatterplot_data(df,labels):
	# Lista di colori per le classi
	colors = [np.random.rand(3,1) for i in set(labels)]

	# Plot coppia di attributi diversi in df 
	attr = [x for x in df]
	for x in attr:
		for y in attr[attr.index(x):]:
			if x != y: #df
				for c in set(labels):
					clusters = zip(df[x],df[y],labels)
					
					cl = [elem for elem in clusters if elem[2] == c]
				
					if c != -1:
						if set(labels).pop() == 1:
							c = colors[cl[0][2]-1]
						else:
							c = colors[cl[0][2]-1]
						s = 100
					else:
						# DBSCAN Nois più piccoli e di colore nero
						c = "Black"
						s = 20
					if len(cl) > 0:
						a = [a[0] for a in cl]
						b = [b[1] for b in cl]
						plt.scatter(a, b, c=c, s=s, label="Claster " + str(cl[0][2]))
						
				plt.xlabel(x)
				plt.ylabel(y)
				plt.legend(loc="center left", bbox_to_anchor=(1, 0.5),fancybox=True, shadow=True,scatterpoints=1)
				#plt.savefig('/tmp/'+x+"-"+y+'.png', bbox_inches='tight')
				#plt.close()
				plt.show()
				
# From http://web.stanford.edu (installare modulo seaborn)
def correlation(df):
	corr = df.corr()
	# Generate a mask for the upper triangle
	mask = np.zeros_like(corr, dtype=np.bool)
	mask[np.triu_indices_from(mask)] = True

	# Set up the matplotlib figure
	f, ax = plt.subplots(figsize=(11, 9))

	# Generate a custom diverging colormap
	cmap = sns.diverging_palette(220, 10, as_cmap=True)

	# Draw the heatmap with the mask and correct aspect ratio
	sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3,square=True,linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
	#plt.savefig('/tmp/correlation.png', bbox_inches='tight')
	#plt.close()
	plt.show()

def percent_clusters(clusters):
	num_class = Counter(clusters)

	print("\nPercentuale elementi nel cluster")
	for key in num_class:
		print("Cluster ",key,": %.1f (%d elementi)" %(((num_class[key]*100)/len(clusters)),num_class[key]))

	# Elementi per claster più grande
	#max_cluster = max([num_class[key] for key in num_class])

if __name__ == "__main__":
	main()
