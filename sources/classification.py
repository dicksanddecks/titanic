import numpy as np
import subprocess
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import tree
from sklearn import metrics
from sklearn.cross_validation import train_test_split

def main():
	# Load train e test
	train = pd.read_csv('../dataset/train_fixed_survived.csv', skipinitialspace=True)
	test = pd.read_csv('../dataset/test_fixed.csv', skipinitialspace=True)
	truth = pd.read_csv('/tmp/truth.csv', skipinitialspace=True)

	train = train.drop("Ticket",1)
	test = test.drop("Ticket",1)


	# Train senza attributo target
	x = train.drop("Survived",1)
	# Attributo target
	y = train["Survived"]

	
	# Costruisco l'albero
	clf = tree.DecisionTreeClassifier(criterion="gini", splitter="best",max_depth=3, min_samples_split=2,min_samples_leaf=46).fit(x,y)

	#Effettuo la previsione
	pred = clf.predict(test)

	#Visualizzo l'albero
	visualize_tree(clf,train.columns,"/tmp/")

	#save_prediction(pred,"../dataset/prediction.csv")

# Salva la previsione su file csv secondo richiesta kaggle
# pred - DataFrame contente la previsione
# path - percorso + nome file
def save_prediction(pred,path):
	test_origin = pd.read_csv('../dataset/test.csv', skipinitialspace=True)
	
	d = {"PassengerId" : test_origin["PassengerId"], "Survived" : pred}
	prediction = pd.DataFrame(d,columns=["PassengerId","Survived"])
	
	prediction.to_csv(path,index=False)

# From http://chrisstrelioff.ws
# IMPORTANTE: necessita pacchetto dot installato sul sistema
# Crea immagine png dell'albero di decisione
# clf - Albero di decisione
# feature_name - Attributi
# path - Percorso in cui salvare l'immagine
def visualize_tree(clf, feature_names, path):
	with open("/tmp/dt.dot", 'w') as f:
		tree.export_graphviz(clf, out_file=f,feature_names=feature_names)

	command = ["dot", "-Tpng", "/tmp/dt.dot", "-o", path+"dt.png"]
	try:
		subprocess.check_call(command)
		subprocess.check_call(["rm", "/tmp/dt.dot"])
	except:
		exit("Subprocess command error")
if __name__ == "__main__":
	main()
