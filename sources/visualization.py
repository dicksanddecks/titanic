import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

__author__ = "Dicks and Decks"

def main():
   
	input_filename = ('../dataset/train.csv')
	dataset = pd.read_csv(input_filename, skipinitialspace=True)
	#dataset = dataset.loc[dataset["Dbscan"] == 13]	

	#-------------------frequencies of men and women aboard--------------------
	print ("\n------------Frequenze di uomini e donne a bordo------------")
	genderFreq = dataset["Sex"].value_counts()
	print (genderFreq)

	#plotting delle frequenze di uomini e donne
	genderFreq.plot(kind='bar', color='r', alpha=0.7)
	plt.ylabel("Frequenza")
	plt.xlabel("Sesso")
	#plt.savefig('/tmp/sex.png', bbox_inches='tight') #salvataggio plot
	plt.show()

	#------------------classes' frequences--------------------
	print ("\n------------Frequenze delle classi------------")
	print ("1 - Prima Classe \t 2 - Seconda Classe \t 3 - Terza Classe\n")	
	print (dataset["Pclass"].value_counts( sort = False ))

	#plotting number of passengers, for each class

	dataset["Pclass"].value_counts().plot(kind='bar', color='r', alpha=0.7)
	plt.ylabel("Frequenza")
	plt.xlabel("Classe della cabina")
	#plt.savefig('/tmp/pClass.png', bbox_inches='tight')
	plt.show()


	#------------------frequencies of embarkment port, number of people embarked for each port----------------------	
	print ("\n------------Frequenze dei porti di partenza------------")
	print ("C - Cherbourg \t Q = Queenstown \t S = Southampton\n")
	print (dataset["Embarked"].value_counts( sort = False ))


	dataset["Embarked"].value_counts().plot(kind = "bar", color = "b", alpha = 0.7)
	plt.ylabel("Numero di persone imbarcate")
	plt.xlabel("Porto d'imbarco")
	#plt.savefig('/tmp/embark.png', bbox_inches='tight')
	plt.show()

	#------------------Age description and statistics----------------------------

	age = dataset["Age"] 
	
	print ("\n------------Statistiche sulle eta'------------\n")
	print (age.describe()) #statistics on age attribute
	print ("Median: %f" % np.median(age))
	numAge = age.count() #number of age values (not null)
	binNumber = np.log2(numAge) + 1 #Sturges' rule for determine the number of bins 
	print ("\n----Frequenze delle eta'------------\n")
	print (age.value_counts(sort = False, bins = binNumber))
	#age.hist(bins = int(binNumber)) #histogram of ages based on Sturges' rule
	#plt.show()
	#age.plot(kind = "box")
	plt.boxplot(age.values)
	plt.ylabel("Eta'")
	#plt.savefig('/tmp/age.png', bbox_inches='tight')
	plt.show()
	#-----------------Conteggio persone che si conoscono------------------------
	#Contiene un dizionario con key = numero di persone che ogni passeggero conosce
	#							value = frequenza per key
	
	count = 0
	spCount = dict()
	for index, item in dataset.iterrows():
		count = item["SibSp"] + item["Parch"]
		if count in spCount:
			spCount[count] += 1
		else:
			spCount[count] = 1
		count = 0
	print("\nFrequenze delle persone che ogni passeggero conosce:")
	print (spCount)
	
	plt.bar(spCount.keys(), spCount.values(), width = 1, color='g')
	plt.ylabel("Frequenza")
	plt.xlabel("Numero di famigliari")
	#plt.savefig('/tmp/sibSpParch.png', bbox_inches='tight')
	plt.show()
	
	#----------------Statistiche su fare----------------------------------------
	
	print ("\n------------Statistiche sui prezzi------------\n")
	
	fare = dataset["Fare"]
	print(fare.describe())	
	print ("Median: %f" % np.median(fare)) #statistics on fare attribute
	numFare = fare.count() #number of age values (not null)
	binNumber = np.log2(numFare) + 1 #Sturges' rule for determine the number of bins 
	print ("\n-----Frequenze dei prezzi------------\n")
	print (fare.value_counts(sort = False, bins = binNumber))
	#fare.hist(bins = int(binNumber)) #histogram of ages based on Sturges' rule
	#fare.plot(kind = "box")
	#plt.show()
	plt.boxplot(fare.values)
	plt.ylabel("Prezzo")
	#plt.savefig('/tmp/fare.png', bbox_inches='tight')
	plt.show()

if __name__ == "__main__":
	main()
