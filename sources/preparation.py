import numpy as np
import pandas as pd
import math
from sklearn import preprocessing
from collections import Counter

def main():
	df = pd.read_csv('../dataset/train.csv', skipinitialspace=True)

	# Sostituisco missing values embarked con classe più frequente
	df["Embarked"] = df["Embarked"].fillna('S')

	# Categorici attributo Embarked in numerici
	df["Embarked"] = df["Embarked"].replace("C",1)
	df["Embarked"] = df["Embarked"].replace("Q",2)
	df["Embarked"] = df["Embarked"].replace("S",3)

	# Categorici attributo Sex in numerici
	df["Sex"] = df["Sex"].replace("female",1)
	df["Sex"] = df["Sex"].replace("male",2)

	# Elimino lettere e simboli per l'attributo Ticket
	df = ticket_to_number(df)

	# Sostituisco missing values Age
	df = titleMedia(df)

	# Elimino record dove attributo Ticket è un missing values
	df = df.dropna(subset=["Ticket"], how="any")

	# Elimino attributi non necessari
	df = df.drop("Name",1)
	df = df.drop("Cabin",1)
	df = df.drop("PassengerId",1)
	#df = df.drop("Survived",1)
	
	#df["Embarked"] = df["Embarked"].convert_objects(convert_numeric=True)

	# Utilizzato per testset
	#df["Fare"] = df["Fare"].fillna(df["Fare"].sum(skipna=True)/len(df["Fare"])-1)
	#df["Age"] = df["Age"].fillna(df["Age"].sum(skipna=True)/len(df["Age"])-1)
	
	df = average_tickets(df)
	
	df.to_csv('../dataset/train_fixed_survived.csv',index=False)

# Elimina lettere da tutti i record dell'attributo Ticket
def ticket_to_number(df):
	ticket = df["Ticket"]
	i=0
	for elem in ticket:
		e = elem.split(" ")
		ticket.loc[i] = e[len(e)-1]
		i += 1
	df["Ticket"] = df["Ticket"].convert_objects(convert_numeric=True)
	return df

#----------Creo una lista con tutte le persone di cui non si conosce l'eta
def noAge(df):

	age = df["Age"]
	listNoAge = []
	count = 0
	for item in age:
		count += 1
		if math.isnan(item):
			listNoAge.append(df["Name"][count-1])
			#print(dataset["Name"][count-1])
	
	print(len(listNoAge))
	print(listNoAge)
	
	return listNoAge


#-------incremento i rispettivi contatori dei titoli per conoscere per ognuno 
#-------di loro a quante persone manca l'attributo eta'	
def titleNoAge(lista):

	mr = 0
	mrs = 0
	miss = 0
	master = 0
	don = 0
	dr = 0

	for name in lista:
		if "Mr" not in name and "Mrs" not in name and "Miss" not in name and "Master" not in name and "Don" not in name:
			print(name)
		if "Mr." in name:
			mr += 1
		elif "Mrs." in name:
			mrs += 1
		elif "Miss." in name:
			miss += 1
		elif "Master." in name:
			master += 1
		elif "Don." in name:
			don += 1
		elif "Dr." in name:
			dr += 1
	
	print("Mr senza eta': %d" %mr)
	print("Mrs senza eta': %d" %mrs)
	print("Miss senza eta': %d" %miss)
	print("Master senza eta': %d" %master)
	print("Don senza eta': %d" %don)
	print("Dr senza eta': %d" %dr)
			
#---- per ogni titolo creo un contatore e un vettore, il primo indica la somma dell'eta' 
#---- di tutte le persone di ciascun titolo, nel secondo ci sara' un elenco di tutte le persone
#---- di cui e' specificata l'eta', ed infine calcolo la media per ogni titolo
def titleMedia(df):
	
	name = df["Name"]
	
	ageMr = []
	ageMrs = []
	ageMiss = []
	ageDr = []
	ageMaster = []

	count = 0
	
	for item in name:
		count += 1
		if "Mr." in item:
			if not math.isnan(df["Age"][count-1]):
				ageMr.append(df["Age"][count-1])
		elif "Mrs." in item:
			if not math.isnan(df["Age"][count-1]):
				ageMrs.append(df["Age"][count-1])
		elif "Miss." in item:
			if not math.isnan(df["Age"][count-1]):
				ageMiss.append(df["Age"][count-1])
		elif "Dr." in item:
			if not math.isnan(df["Age"][count-1]):
				ageDr.append(df["Age"][count-1])
		elif "Master" in item:
			if not math.isnan(df["Age"][count-1]):
				ageMaster.append(df["Age"][count-1])
	
	mediaMr = int(sum(ageMr)/len(ageMr))
	mediaMrs = int(sum(ageMrs)/len(ageMrs))
	mediaMiss = int(sum(ageMiss)/len(ageMiss))
	mediaDr = int(sum(ageDr)/len(ageDr))
	mediaMaster = int(sum(ageMaster)/len(ageMaster))
		
#---- una volta calcolata la media per ogni titolo, sostituisco i valori mancanti 
#---- in maniera opportuna con le medie calcolate precedentemente
	var = 0
	for item in df["Name"]:	
		var += 1
		if 'Mr.' in item:
			if math.isnan(df["Age"][var-1]):
				df["Age"][var-1] = mediaMr
		
		elif 'Mrs.' in item:
			if math.isnan(df["Age"][var-1]):
				df["Age"][var-1] = mediaMrs
		
		elif 'Miss.' in item:
			if math.isnan(df["Age"][var-1]):
				df["Age"][var-1] = mediaMiss
			
		elif 'Dr.' in item:
			if math.isnan(df["Age"][var-1]):
				df["Age"][var-1] = mediaDr
				
		elif 'Master' in item:
			if math.isnan(df["Age"][var-1]):
				df["Age"][var-1] = mediaMaster
				
	return df


def average_tickets(df):
	ticket = Counter(df["Ticket"])
	for index,row in df.iterrows():
		df.ix[index,"Fare"] = row["Fare"]/ticket[row["Ticket"]]
	return df

if __name__ == "__main__":
	main()
